ARG PYTHON_VERSION

FROM python:${PYTHON_VERSION}

RUN apt-get update \
    && apt-get install -yqq \
    git \
    vim