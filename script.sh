#!/usr/bin/env bash
pip install -r /home/requirements.txt
ln -s /usr/local/bin /home/vendor/bin && ln -s /usr/local/lib /home/vendor/lib
python manage.py migrate
python manage.py runserver 0.0.0.0:8000